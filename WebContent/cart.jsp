<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.usuario}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Carrito</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
</head>
<body class='bg-dark'>
<div class='container-fluid m-4'>
	<h1 style='color:white; text-align:center;'>Carrito de la compra</h1>
	<p style='margin-top: 20px; text-align:center;'>
		<a href="catalogo.jsp" class='btn btn-danger'>Catálogo</a>
	</p>
	<c:choose>
		<c:when test="${empty sessionScope.cart}">
			<p>No se han añadido productos al carrito</p>
		</c:when>
		<c:otherwise>
		<div style='margin-top: 60px; width:700px; color:white; margin:auto;'class='border'>
			<table style='margin:auto; text-align:center;'>
				<tr>
					<th style='width:25%; color:red;'>Producto</th>
					<th style='width:25%; color:red;'>Unidades</th>
					<th style='width:25%; color:red;'>Precio</th>
					<th style='width:25%; color:red;'>Importe</th>
				</tr>
				<c:forEach var="linea" items="${sessionScope.cart}">
					<tr>
						<td>${linea.value.productName}</td>
						<td>${linea.value.amount}</td>
						<td>${linea.value.price}</td>
						<td>${linea.value.amount * linea.value.price}</td>
					</tr>
				</c:forEach>
			</table>
			</div>
		</c:otherwise>
	</c:choose>
	</div>
</body>
</html>
