<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<sql:query var="modelos" dataSource="jdbc/classicmodels">
	select productCode, productName, productScale, productDescription, MSRP from products;
</sql:query>


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Catálogo de Productos</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<style>
.colorNombre {
	color: #4EC0EC;
		margin:auto;
	text-align: center;
}

.colorCol {
	color: #858585;
}

.carro{
	margin:auto;
	text-align: right;
}


.centrar {
	margin:auto;
	text-align: center;
}

.textoUsu {
	color: white;
	text-align: center;
}
</style>
</head>
<body class="bg-dark">
	<h1 class="text-light text-center p-4">Catálogo de Modelos a
		Escala</h1>

	<hr />

	<c:choose>
		<c:when test="${not empty sessionScope.usuario}">
			<p class="colorNombre">Bienvenido/a ${sessionScope.usuario}</p>
			<p class="carro"><a href="cart.jsp"  class="btn btn-primary m-2" role="button">Carrito</a></p>
			<p class="centrar">
				<a href="logout.jsp" class="btn btn-danger mb-2" role="button">Cerrar Sesion</a>
			</p>
		</c:when>
		<c:otherwise>
			<p class="centrar">
				<a href="registro.jsp" class="btn btn-success mb-2" role="button">Registro</a>
			</p>
			<p class="centrar">
				<a href="login.jsp" class="btn btn-danger mb-2" role="button">Iniciar Sesion</a>
			</p>

		</c:otherwise>
	</c:choose>


<div class="container-fluid">

	<table class="table table-striped table-dark text-center">
		<thead>
			<tr>
				<th scope="col" class="colorCol">#</th>
				<th scope="col" class="colorNombre">Escala / Precio</th>
				<th scope="col" class="colorNombre">Descripción</th>
				<th scope="col" class="colorNombre"></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="modelo" items="${modelos.rows}">
				<tr>
					<th scope="row" class="w-25 colorNombre"><c:out
							value="${modelo.productName}" /></th>
					<td scope="row" class="w-25"><c:out
							value="${modelo.productScale}" /> / <c:out
							value="${modelo.MSRP}" />$</td>
					<td scope="row" class="w-20"><c:out
							value="${modelo.productDescription}" /></td>

					<td scope="row" class="w-5">
						<c:if test="${not empty sessionScope.usuario}">
							<a
								href="cart?c=${modelo.productCode}&n=${modelo.productName}&p=${modelo.MSRP}">
								<img style="width: 2em; height: 2em;" src="https://img.icons8.com/carbon-copy/100/ffffff/shopping-cart-loaded.png" />
							</a>
						</c:if></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
</body>
</html>