<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>

<c:if test="${not empty sessionScope.usuario}">
	<c:redirect url="catalogo.jsp" />
</c:if>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<style>

	.formu {
	margin: auto;
	text-align: center;
	background-color: #40525A;
	width: 250px;
	padding: 10px;
		border-radius: 20px;
	border: 1px solid black;
	
	}
	
	label{
		color: #D1D5DC;
	}
	
</style>
<title>Registro de Usuarios</title>
<script>
	function check(confirm) {
		if (confirm.value != document.getElementById('password').value) {
			confirm.setCustomValidity('Las contraseñas no coinciden');
		} else
			confirm.setCustomValidity('');
	}
</script>
</head>
<body class='bg-dark'>
	<form action="signup" method="post" onsubmit="check()">
		<h1 style="text-align: center; color:white;">Registro de usuario</h1>
		<hr />

	<div class="formu">
		<p>
			<label for="nombre">Nombre</label>
		</p>
		<p>
			<input type="text" name="nombre" required="required" />
		</p>

		<p>
			<label for="apellidos">Apellidos</label>
		</p>
		<p>
			<input type="text" name="apellidos" required="required" />
		</p>

		<p>
			<label for="email">eMail</label>
		</p>
		<p>
			<input type="email" name="email" required="required" />
		</p>

		<p>
			<label for="password">Contraseña</label>
		</p>
		<p>
			<input type="password" name="password" id="password"
				required="required" />
		</p>

		<p>
			<label for="password">Confirmar contraseña</label>
		</p>
		<p>
			<input type="password" id="confirm" required="required"
				oninput="check(this)" />
		</p>
	</div>
	
	<div class="row justify-content-center">
			<p class="p-2">
				<button class="btn btn-success" type="submit" value="Registrar">Registrar</button>
			</p>
	</div>

	</form>
		<c:choose>
		<c:when test="${param.status == 1}">
			<p>El email ya está registro</p>
		</c:when>
		<c:when test="${param.status == 2}">
			<p>Error del sistema, contacte con el administrador</p>
		</c:when>
	</c:choose>
	
</body>
</html>
