package fp.daw.classicmodels;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailService {

	public static void sendMessage(String smtpServer, String port, String account, String password, String to,
			String subject, String body, String type) throws MessagingException {
		Properties properties = new Properties();
		// Servidor SMTP.
		properties.setProperty("mail.smtp.host", smtpServer);
		// Habilitar TLS.
		properties.setProperty("mail.smtp.starttls.enable", "true");
		// Puerto para envío de correos.
		properties.setProperty("mail.smtp.port", port);
		// Si requiere usuario y password para conectarse.
		if (account != null && password != null)
			properties.setProperty("mail.smtp.auth", "true");
		// Obtener un objeto Session con las propiedades establecidas.
		Session mailSession = Session.getInstance(properties);
		// Para obtener un log de salida más extenso.
		mailSession.setDebug(true);
		Message message = new MimeMessage(mailSession);
		message.setFrom(new InternetAddress(account));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		message.setSubject(subject);
		message.setContent(body, type);
		message.setSentDate(new Date());
		Transport.send(message, account, password);
	}
	
	
	public static void sendConfirmationMessage(String email, String confirmationCode) throws MessagingException {
		StringBuilder body = new StringBuilder();

		body.append("<!DOCTYPE html>\r\n"
				+ "		<html>\r\n"
				+ "<head>\r\n"
				+ "<meta charset=\"ISO-8859-1\">\r\n"
				+ "<title>Post-Registro</title>\r\n"
				+ "<link rel=\"stylesheet\"\r\n"
				+ "	href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css\">\r\n"
				+ "<script\r\n"
				+ "	src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\r\n"
				+ "<script\r\n"
				+ "	src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js\"></script>\r\n"
				+ "<script\r\n"
				+ "	src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js\"></script>\r\n"
				+ "</head>\r\n"
				+ "<html>\r\n"
				+ "<body class=\"container\">");
		body.append("<div class=\"container-fluid\">");
		body.append("<p>Pulsa el el botón para validar tu cuenta y poder acceder a nuestros servicios</p>");
		body.append("<form action=\"http://localhost:8080/ClassicModels/confirm\"");
		body.append(" method=\"post\" class=\"form-group text-center\">");
		body.append("<input type=\"hidden\" name=\"email\" value=\"");
		body.append(email);
		body.append("\" />");
		body.append("<input type=\"hidden\" name=\"cc\" value=\"");
		body.append(confirmationCode);
		body.append("\" />");
		body.append("<p><input type=\"submit\" class=\"btn btn-primary\" value=\"Confirmar\" /></p>");
		body.append("</form>");
		body.append("</div>");
		body.append("</body>");
		body.append("</html>");


		sendMessage(
				"smtp.gmail.com", // servidor SMPT
				"587", // puerto
				"classicmodelsfp@gmail.com", // cuenta para iniciar sesión en el servidor SMTP de Gmail
				"FP2021cifp", // contraseña para iniciar sesión en el servidor SMTP de Gmail
				email, // dirección del destinatario
				"Confirma tu dirección de correo electrónico", // asunto
				body.toString(), // cuerpo del mensaje
				"text/html" // tipo de contenido
				);
	}


}
