<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
	
<c:choose>
	<c:when test="${not empty sessionScope.usuario }">
		<c:redirect url="catalogo.jsp" />
	</c:when>
	<c:otherwise>
<%
String firstName = request.getParameter("nombre");
String email = request.getParameter("email");

if (firstName == null || email == null) 
	response.sendRedirect("catalogo.jsp");
else{
%>


		<!DOCTYPE html>
		<html>
<head>
<meta charset="ISO-8859-1">
<title>Post-Registro</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
	<section class="container">
	
		<div class="container-fluid">
			<h1>
				Bienvenid@
				<%
			if (firstName != null) {
				out.println(firstName);
			} else {
				if (session.getAttribute("nombre") != null) {
					out.println(session.getAttribute("nombre"));
				} else {
					response.sendRedirect("catalogo.jsp");
				}
			}
			%>
			</h1>
			<p>
				Sólo queda un paso más para completar tu registro y tener acceso a
				todos nuestros servicios, sigue las instrucciones que te hemos
				enviado a <strong> <%
 if (email != null) {
 	out.println(email);
 } else {
 	if (session.getAttribute("email") != null) {
 		out.println(session.getAttribute("email"));
 	} else {
 		response.sendRedirect("catalogo.jsp");
 	}
 }
 %>
				</strong>
				<p>
					<button class="btn btn-primary" href="login.jsp">Iniciar Sesión</button>
					<button class="btn btn-danger" href="registro.jsp">Regístrate</button>
				</p>
			<p>Si no has recibido el mensaje pulsa el botón para Reenviar el
				código de confirmación</p>
			<form action="reenvio" method="post">
				<div class="form-group text-center">
					<input type="hidden" name="email" value="<%out.print(email);%>" />
					<input type="submit" class="btn btn-primary" value="Reenviar" />
				</div>
			</form>
		</div>
	</section>
</body>
		</html>
		<%
		}
		%>
	</c:otherwise>
</c:choose>


