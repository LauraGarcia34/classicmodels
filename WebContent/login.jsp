<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true"%>

<c:if test="${not empty sessionScope.usuario }">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inicio de Sesi�n</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<style>
.formulario {

	width: 400px;
	padding-top: 10px;
	padding-bottom: 10px;
	margin: auto;
	text-align: center;
	background-color: #40525A;
	color:white;
	border-radius: 20px;
	border: 1px solid black;
}

label {
	display: inline-block;
	width: 100px;
}

input {
	border: 1px solid black;
}
</style>
</head>
<body class='bg-dark'>
<div style='margin-top: 100px;'>
	<div class="formulario">
		<form action="auth" method="post">
			<p>
				<label for="email">eMail: </label> <input type="email" name="email"
					required="required" />
			</p>
			<p>
				<label for="password">Contrase�a: </label> <input type="password"
					name="password" required="required" />
			</p>
			<p>
				<input type="submit" class='btn btn-danger' value="Iniciar sesion" />
			</p>
		</form>
		<c:choose>
			<c:when test="${ param.status == 1}">
				<p>Las credenciales del usuario no son correctas</p>
			</c:when>

			<c:when test="${param.status == 2}">
				<p>Error del sistema, ponte en contacto con el administrados.</p>
			</c:when>

		</c:choose>

	</div>
	</div>
</body>
</html>
